package com.example.sample1.test;

import com.example.sample1.MainActivity;

import android.test.ActivityInstrumentationTestCase2;

/**
 * hellojunitプロジェクトのテスト。
 *
 */
import com.example.sample1.MainActivity;
import com.example.sample1.R;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

/**
 * hellojunitプロジェクトのテスト。
 *
 */
public class TestMain extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mActivity;
    private EditText et1;
    private EditText et2;
    private Button btn1;


    // 必須のコンストラクタ
    public TestMain() {
        super(MainActivity.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // アクティビティを取得
        mActivity = getActivity();

        et1 = (EditText)mActivity.findViewById(R.id.et1);
        et2 = (EditText)mActivity.findViewById(R.id.et2);
        btn1 = (Button)mActivity.findViewById(R.id.btn1);

    }


    public void testAddHelloLogic() {
        assertEquals(mActivity.addHello("山田"), "Hello, 山田!");
    }


    public void testUI() {
        // 最初は空
        assertEquals(et1.getText().toString().length(), 0);
        assertEquals(et2.getText().toString().length(), 0);

        assertTrue(btn1.isEnabled());


        // UIスレッド上で画面操作
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                et1.setText("World");

                btn1.performClick();

                assertEquals(et2.getText().toString(), "Hello, World!");
            }
        });
        // UIスレッドが終了するまで待つ
        getInstrumentation().waitForIdleSync();

    }

}
